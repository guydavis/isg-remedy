using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using System.Net.Mail;
using System.Net;
using System.Reflection;

namespace ISGRemedy.Features.RemedyEmails
{
	/// <summary>
	/// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
	/// </summary>
	/// <remarks>
	/// The GUID attached to this class may be used during packaging and should not be modified.
	/// </remarks>

	public class ProcessLibraryEvent : SPItemEventReceiver
	{
		public override void ItemUpdated(SPItemEventProperties properties)
		{
			EventFiringEnabled = false;
            base.ItemUpdated(properties);
			if (properties.ListItem["RemedyEmailToSend"].ToString() != "None")
			{
				SendRemedyEmail(properties);
				properties.ListItem["RemedyEmailToSend"] = "None";
				properties.ListItem.SystemUpdate();
			}

			EventFiringEnabled = true;
		}

		private SPListItem GetEmailHeader(SPItemEventProperties properties)
		{
			string type = properties.ListItem["RemedyEmailToSend"].ToString();
			string list = properties.ListTitle;
			string form = string.Empty;
			SPListItem header = null;

			switch (list)
			{
				case "Firewall iChain Proxy Change":
					form = "306 Form";
					break;
				case "MSBStandards":
					form = "MSB Form";
					break;
				case "Non-User / Elevated Account Provisioning":
					form = "409 Form";
					break;
				case "RemovableMedia":
					form = "Safe End Form";
					break;
			}

			SPQuery query = new SPQuery();
			query.Query = string.Format(string.Format(@"
						<Where>
							<And>
								<BeginsWith><FieldRef Name=""Title"" /><Value Type=""Text"">{0}</Value></BeginsWith>
								<Eq><FieldRef Name=""RequestType"" /><Value Type=""Text"">{1}</Value></Eq>
							</And>
						</Where>", form, type));

			SPWeb site = properties.Web as SPWeb;
			SPList remedyList = site.Lists.TryGetList("RemedyEmails");
			if (remedyList!= null)
			{
				SPListItemCollection items = remedyList.GetItems(query);
				if (items.Count > 0)
				{
					header = items[0];
				}
			}

			return header;
		}

		private void SendRemedyEmail(SPItemEventProperties properties)
		{
			SPListItem header = GetEmailHeader(properties);
			if (header != null)
			{
				BuildAndSendEmail(header, properties);
			}
		}

		private void BuildAndSendEmail(SPListItem header, SPItemEventProperties properties)
		{
			MailMessage msg = new MailMessage();
			msg.BodyEncoding = System.Text.Encoding.ASCII;
			if (header["EmailTo"] != null)
			{
				msg.To.Add(header["EmailTo"].ToString());
			}
			else
			{
                msg.To.Add("SharePointAdmin@lowes.com");
			}
            msg.From = new MailAddress("SharePointAdmin@lowes.com", "SharePointAdmin");
			msg.Subject = string.Format("Remedy Ticket: {0}", properties.ListItem["Name"].ToString());

			string firstName = "NonFirstName";
			string lastName = "NonLastName";
			string requestor = properties.ListItem["Requestor_x0020_Name"] == null ? string.Empty : properties.ListItem["Requestor_x0020_Name"].ToString();
			string name = properties.ListItem["Requestor_x0020_Name"] == null ? string.Empty : properties.ListItem["Name"].ToString();
			name = name.Replace(".xml", string.Empty);
			name = name.Substring(name.IndexOf('-') + 1);

			if (requestor.Length > 0)
			{
				var names = requestor.Split(' ');
				firstName = names[0];
				if (names.Length > 1)
				{
					lastName = string.Empty;
					for (int i = 1; i < names.Length; i++)
					{
						lastName += names[i] + " ";
					}
					lastName = lastName.Trim();
				}
			}

			string nameItemForm = string.Format(@"http://sp2013.lowes.com/sites/secrequests/_layouts/15/FormServer.aspx?XmlLocation={0}", properties.ListItem["EncodedAbsUrl"].ToString());
			string emailBody = header["Header"].ToString();

			emailBody = string.Format(emailBody, firstName, lastName, nameItemForm, name);
			emailBody = emailBody.Replace("\n", string.Empty).Replace("\r", string.Empty);

			msg.Body = emailBody;
			msg.IsBodyHtml = false;
			msg.Headers.Add("content-type", "text/plain");
			SmtpClient smtp = new SmtpClient("Outlook-usnccorp01.lowes.com");
			smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
			smtp.Send(msg);
		}
	}

	[Guid("bc8a7849-cd7a-4ad9-8af6-5318d41c99df")]
	public class RemedyEmailsEventReceiver : SPFeatureReceiver
	{
		// Uncomment the method below to handle the event raised after a feature has been activated.

		public override void FeatureActivated(SPFeatureReceiverProperties properties)
		{
			AddEventReceivers(properties);
		}

		private void AddEventReceivers(SPFeatureReceiverProperties properties)
		{
			SPWeb site = properties.Feature.Parent as SPWeb;
			site.Lists["Firewall iChain Proxy Change"].EventReceivers.Add(
				SPEventReceiverType.ItemUpdated, Assembly.GetExecutingAssembly().FullName,
				"ISGRemedy.Features.RemedyEmails.ProcessLibraryEvent");
			site.Lists["Non-User / Elevated Account Provisioning"].EventReceivers.Add(
				SPEventReceiverType.ItemUpdated, Assembly.GetExecutingAssembly().FullName,
				"ISGRemedy.Features.RemedyEmails.ProcessLibraryEvent");
			site.Lists["RemovableMedia"].EventReceivers.Add(
				SPEventReceiverType.ItemUpdated, Assembly.GetExecutingAssembly().FullName,
				"ISGRemedy.Features.RemedyEmails.ProcessLibraryEvent");
			site.Lists["MSBStandards"].EventReceivers.Add(
				SPEventReceiverType.ItemUpdated, Assembly.GetExecutingAssembly().FullName,
				"ISGRemedy.Features.RemedyEmails.ProcessLibraryEvent");
		}


		// Uncomment the method below to handle the event raised before a feature is deactivated.

		public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
		{
			RemoveEventReceivers(properties);
		}

		private void RemoveEventReceivers(SPFeatureReceiverProperties properties)
		{
			string projectAssembly = Assembly.GetExecutingAssembly().FullName;
			SPWeb site = properties.Feature.Parent as SPWeb;
			SPListCollection lists = site.Lists;
			for (int i = lists.Count - 1; i >= 0; i--)
			{
				SPEventReceiverDefinitionCollection eventReceivers = lists[i].EventReceivers;
				for (int j = eventReceivers.Count - 1; j >= 0; j--)
				{
					SPEventReceiverDefinition erd = eventReceivers[j];

					if (erd.Assembly == projectAssembly)
					{
						erd.Delete();
					}
				}
			}
		}

		// Uncomment the method below to handle the event raised after a feature has been installed.

		//public override void FeatureInstalled(SPFeatureReceiverProperties properties)
		//{
		//}


		// Uncomment the method below to handle the event raised before a feature is uninstalled.

		//public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
		//{
		//}

		// Uncomment the method below to handle the event raised when a feature is upgrading.

		//public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
		//{
		//}
	}
}
